L’algorithme des k plus proches voisins (ou kNN : *k nearest neightbours* en anglais) permet de déterminer à quel classe appartient un objet en le comparant avec d’autres objets issus d’un ensemble de référence et déjà classés.

En fait, kNN peut nous fournir deux types d’informations :

* une **classification** de l’objet (voir les deux exemples précédents) ;
* une **prédiction** relative à une caractéristique de l’objet (exemple : la longueur des cheveux du personnage mystère, en faisant la moyenne de celles de ses plus proches voisins).

Nous ne travaillerons que sur la question de la classification d'un nouvel objet.


Sur l'animation ci-dessous :

* choisir le nombre de classes,
* placer la donnée 𝑢,
* afficher les 𝑘 plus proches voisins,
* cliquer sur l'icone en haut à droite pour redistribuer les données.


<iframe loading="lazy" width="460px" height="460px" style="border: 0px;" scrolling="no" title="k plus proches voisins" src="https://www.geogebra.org/material/iframe/id/q3hq97tw/width/460/height/460/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/false/sdz/false/ctl/false"> </iframe>

[Source](https://info.blaisepascal.fr/nsi-les-k-plus-proches-voisins)