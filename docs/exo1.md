Nous travaillons sur l'exemple 2 (Trolls et Elfes).

Le tableau des 26 personnages peut être modélisé en Python par une liste de tuples :

```python
personnages = [(14, 10, 2), (9, 11, 2), (10, 14, 2), (8, 28, 2), (1, 31, 1), (14, 4, 2), (13, 20, 2), 
            (4, 24, 1), (5, 34, 1), (1, 22, 1), (9, 30, 1), (13, 15, 2), (8, 40, 1), (9, 18, 2), 
            (12, 3, 2), (2, 40, 1), (11, 3, 2), (7, 31, 1), (1, 33, 1), (9, 38, 1)]
```

ainsi chaque tuple donne des caractéristiques d'un personnage :

* le premier nombre est sa vitesse ;
* le deuxième nombre est sa puissance ;
* le troisième nombre est sa classe : 1 pour un Troll et 2 pour un Elfe.

1°) Complétez le script suivant qui isole les caractéristiques des personnages dans quatre listes :

{{ IDE('exo1') }}

??? tip "Indication"
    Utilisez des listes en compréhension...


Vous pouvez maintenant exécuter les deux cellules suivantes :

{{ IDE('exo1b') }}

{{ IDE('exo1c') }}