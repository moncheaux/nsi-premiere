Imaginons qu’une machine dotée d’une caméra doive déterminer si un fruit qui lui est présenté est une clémentine ou une orange. La mesure du diamètre de ce fruit est alors un critère pertinent.

Nous dirons ici qu’il y a deux **classes** (clémentine ou orange) et une **caractéristique** (le diamètre). 

Pour que la machine puisse donner son verdict, il faut lui fournir un ensemble de données E, ici les diamètres de quelques fruits dont nous connaissons la classe (dans cet exemple, 7 oranges et 5 clémentines) :

<img style="width:50%; margin:auto;" src="/nsi/premiere/img/knn1.png">

La machine voit un fruit et mesure son diamètre, ce qui nous permet de le positionner :

<img style="width:50%; margin:auto;" src="/nsi/premiere/img/knn2.png">

Pour classer ce fruit, elle va observer ses *k* plus proches voisins :
* pour k = 1 : son voisin le plus proche est une orange donc le fruit serait une orange ;

<img style="width:50%; margin:auto;" src="/nsi/premiere/img/knn3.png">

* pour k = 3 : ses 3 plus proches voisins sont deux clémentines et une orange donc le fruit serait une clémentine ;

<img style="width:50%; margin:auto;" src="/nsi/premiere/img/knn4.png">

* pour k = 5 : ses 5 plus proches voisins sont deux clémentines et trois oranges donc le fruit serait une orange.

<img style="width:50%; margin:auto;" src="/nsi/premiere/img/knn5.png">


**Si l’algorithme choisit de travailler avec 5 voisins** alors il classera le nouveau fruit en tant qu’orange. 
