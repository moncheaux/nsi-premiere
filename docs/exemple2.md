Supposons que des personnages d’un jeu aient deux caractéristiques principales : la vitesse et la puissance.

Parmi les différentes classes de personnages, intéressons-nous à deux d’entre-elles :

* les Trolls, qui sont lents mais très puissants ;
* les Elfes, qui sont rapides mais moins puissants que les Trolls.

Ici il y a deux **classes** (Troll ou Elfe) et deux **caractéristiques** (vitesse et puissance). 

Voici un exemple de relevé de 22 personnages :

Vitesse | Puissance | Classe
:---: | :---: | :---:
14 | 10 | Elfe
9 | 11 | Elfe
10 | 14 | Elfe
8 | 28 | Elfe
1 | 31 | Troll
14 | 4 | Elfe
13| 20 | Elfe
4 | 24 | Troll
5 | 34 | Troll
1 | 22 | Troll
9 | 30 | Troll
13 | 15 | Elfe
8 | 40 | Troll
9 | 18 | Elfe
12 | 3 | Elfe
2 | 40 | Troll
11 | 3 | Elfe
7 | 31 | Troll
1 | 33 | Troll
9 | 38 | Troll

Chaque personnage peut être représenté par un point : en abscisse la vitesse et en ordonnée la puissance :
![](/nsi/premiere/img/knn6.png)

Un adversaire mystère apparaît, il a une vitesse de 8 et une puissance de 26… est-ce un Elfe ou un Troll ?

Pour le classer, observons ses *k* plus proches voisins :

* pour k = 1 : son voisin le plus proche est un Elfe donc le personnage serait un Elfe ;
![](/nsi/premiere/img/knn7.png)
* pour k = 4 : ses 4 plus proches voisins sont trois Trolls et un Elfe donc le personnage serait plutôt un Troll ;
![](/nsi/premiere/img/knn8.png)
* pour k = 5 : même conclusion.

**Si l’algorithme choisit de travailler avec 4 ou 5 voisins** alors le personnage mystère sera classé comme un Troll.

---

!!! note "Remarques"

  * nous aurions aussi pu effectuer une comparaison avec les voisins en ne regardant que la vitesse, ou que la puissance ou en pondérant ces caractéristiques de façon différente (exemple : la vitesse compte pour 70 % et la puissance pour 30 %) ; nous n'aurions pas forcément obtenu la même classe de personnage au final ;

  ![](/nsi/premiere/img/knn10.png)![](/nsi/premiere/img/knn11.png)

  * le rayon des cercles tracés précédemment est-il en km/h ? en Watts ? ni l'un ni l'autre en fait : le calcul de la distance entre deux personnages n'a pas vraiment de signification réelle et peut être largement discuté !