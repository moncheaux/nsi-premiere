L’algorithme des **k plus proches voisins** appartient à la famille des algorithmes d’apprentissage automatique (_machine learning_). L’idée d’apprentissage automatique ne date pas d’hier, puisque le terme de machine learning a été utilisé pour la première fois par l’informaticien américain Arthur Samuel en 1959. Les algorithmes d’apprentissage automatique ont connu un fort regain d’intérêt au début des années 2000 notamment grâce à la quantité de données disponibles sur internet.

L’algorithme des k plus proches voisins est un algorithme d’apprentissage supervisé, il est nécessaire d’avoir des données labellisées. À partir d’un ensemble E de données labellisées, il sera possible de classer (déterminer le label d') une nouvelle donnée qui n’appartient pas à E. 

De nombreuses sociétés (par exemple les GAFAM) utilisent les données concernant leurs utilisateurs afin de ”nourrir” des algorithmes de machine learning qui permettront à ces sociétés d’en savoir toujours plus sur nous et ainsi de mieux cerner nos ”besoins” en termes de consommation.

Citons également le domaine de la reconnaissance d’écriture, de traduction automatique, etc.
Un des domaines en plein essor est celui des véhicules autonomes ; ceux-ci doivent être capable de faire la différence entre un sac plastique et un chien qui traverse une rue par exemple…